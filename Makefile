CC := poetry
PYTHONFILES := snake_game

install:
	$(CC) install

black: $(PYTHONFILES)
	$(CC) run black .

lint: $(PYTHONFILES)
	$(CC) run pylint $(PYTHONFILES)

test: $(PYTHONFILES)
	$(CC) run pytest -vv $(PYTHONFILES)

mypy: $(PYTHONFILES)
	$(CC) run mypy $(PYTHONFILES)

blt: black lint test

.PHONY: black lint test