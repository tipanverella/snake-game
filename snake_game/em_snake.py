"""
	Original version of the snake game
"""
import random
import curses
import sys

# make a text-based window for the game
screen = curses.initscr()
curses.curs_set(False)
screen_height, screen_width = screen.getmaxyx()
window = curses.newwin(screen_height, screen_width, 0, 0)
window.MY_KEYpad(True)
window.timeout(100)

# make a snake
_x = screen_width // 4
_y = screen_height // 2
snake = [[_y, _x], [_y, _x - 1], [_y, _x - 2]]

# make a FOOD piece
FOOD = [screen_height // 2, screen_width // 2]
window.addch(FOOD[0], FOOD[1], curses.ACS_PI)

MY_KEY = curses.KEY_RIGHT

while True:
    next_key = window.getch()
    MY_KEY = MY_KEY if next_key == -1 else next_key

    # player lost the game
    if (
        snake[0][0] in [0, screen_height]
        or snake[0][1] in [0, screen_width]
        or snake[0] in snake[1:]
    ):
        curses.endwin()
        sys.exit()

    new_head = [snake[0][0], snake[0][1]]

    if MY_KEY == curses.KEY_DOWN:
        new_head[0] += 1
    if MY_KEY == curses.KEY_UP:
        new_head[0] -= 1
    if MY_KEY == curses.KEY_LEFT:
        new_head[1] -= 1
    if MY_KEY == curses.KEY_RIGHT:
        new_head[1] += 1

    snake.insert(0, new_head)

    if snake[0] == FOOD:
        # make new FOOD that's not on the snake
        FOOD = None
        while FOOD is None:
            new_food = [
                random.randint(1, screen_height - 1),
                random.randint(1, screen_width - 1),
            ]
            FOOD = new_food if new_food not in snake else None
        window.addch(FOOD[0], FOOD[1], curses.ACS_PI)
    else:
        tail = snake.pop()
        window.addch(tail[0], tail[1], " ")

    window.addch(snake[0][0], snake[0][1], curses.ACS_CKBOARD)
